#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/27 20:41
import unittest
from libs.ddt import ddt, data
from scripts.handle_excel import HandleExcel
from scripts.handle_config import config
from scripts.handle_logger import logger
from scripts.handle_requests import HttpRequests
from scripts.handle_context import Context
from scripts.constance import CASES_PATH
from scripts.handle_mysql import HandleMysql
import json

excel = HandleExcel(CASES_PATH, 'recharge')
cases = excel.get_cases()


@ddt
class TestRecharge(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.do_request = HttpRequests()
        cls.mysql = HandleMysql()
        logger.info('开始执行用例')
        cls.file_name = config.get_value('file path', 'log_path')

    @classmethod
    def tearDownClass(cls):
        cls.do_request.close()
        cls.mysql.close()
        logger.info('执行用例结束')

    @data(*cases)
    def test_recharge(self, one_case):
        """
        先正则替换参数，再发送request请求
        :param one_case:
        :return:
        """
        case_id = one_case['case_id']
        case_url = one_case['url']
        case_data = one_case['data']
        case_method = one_case['method']
        expected = str(one_case['expected'])
        msg = one_case['title']
        check_sql = one_case['check_sql']

        url = config.get_value('requests', 'base_url') + case_url
        # 参数替换
        new_data = Context.recharge_data_replace(case_data)

        if check_sql:
            # 充值前检查下余额
            temp = json.loads(new_data)
            investor_tel = temp['mobilephone']
            recharge_amount = float(temp['amount'])
            money_before = self.mysql.run(check_sql, investor_tel)['LeaveAmount']

        actual = self.do_request.to_request(case_method, url, new_data).text
        try:
            self.assertIn(expected, actual, msg=msg)
            if check_sql:
                money_after = self.mysql.run(check_sql, investor_tel)['LeaveAmount']
                self.assertEqual(recharge_amount, money_after - money_before, msg=msg)
            result = config.get_value('msg', 'success_result')
            excel.write_result(case_id + 1, actual, result)
            logger.info('{},执行结果为：{}'.format(msg, result))
        except AssertionError as e:
            result = config.get_value('msg', 'fail_result')
            excel.write_result(case_id + 1, actual, result)
            logger.error('{},执行结果为：{};具体异常为{}'.format(msg, result, e))
            raise e


if __name__ == '__main__':
    unittest.main()
