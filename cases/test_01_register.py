#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/27 20:41
import unittest
from libs.ddt import ddt, data
from scripts.handle_excel import HandleExcel
from scripts.handle_config import config
from scripts.handle_logger import logger
from scripts.handle_requests import do_request
from scripts.handle_context import Context
from scripts.constance import CASES_PATH

excel = HandleExcel(CASES_PATH, 'register')
cases = excel.get_cases()


@ddt
class TestRegister(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logger.info('开始执行用例')
        cls.file_name = config.get_value('file path', 'log_path')

    @classmethod
    def tearDownClass(cls):
        logger.info('执行用例结束')

    @data(*cases)
    def test_register(self, one_case):
        """
        先正则替换参数，再发送request请求
        :param one_case:
        :return:
        """
        case_id = one_case['case_id']
        case_url = one_case['url']
        case_data = one_case['data']
        case_method = one_case['method']
        expected = one_case['expected']
        msg = one_case['title']
        register_url = config.get_value('requests', 'base_url') + case_url
        # 参数替换
        new_data = Context.register_data_replace(case_data)
        actual = do_request.to_request(case_method, register_url, new_data).text
        try:
            self.assertEqual(expected, actual, msg=msg)
            result = config.get_value('msg', 'success_result')
            excel.write_result(case_id + 1, actual, result)
            logger.info('{},执行结果为：{}'.format(msg, result))
        except AssertionError as e:
            result = config.get_value('msg', 'fail_result')
            excel.write_result(case_id + 1, actual, result)
            logger.error('{},执行结果为：{};具体异常为{}'.format(msg, result, e))
            raise e


if __name__ == '__main__':
    unittest.main()
