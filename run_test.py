#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/26 19:16
from scripts.handle_config import config
import unittest
from libs import HTMLTestRunnerNew
from scripts.constance import REPORT_PATH
from scripts.constance import CASE_PATH
import time
import os
from scripts.handle_user import user
from scripts.constance import NEW_CONF_PATH


if __name__ == '__main__':

    if os.path.exists(NEW_CONF_PATH):
        user.register_3_users_and_save()

    suite = unittest.defaultTestLoader.discover(CASE_PATH)
    # path = REPORT_PATH + '/' + config.get_value('report', 'report_name') + '_' + time.strftime('%Y%m%d_%H%M%S') +
    # '.html'

    path = REPORT_PATH + '/' + config.get_value('report', 'report_name') + '.html'
    with open(path, 'wb') as f:
        runner = HTMLTestRunnerNew.HTMLTestRunner(stream=f,
                                                  title=config.get_value('report', 'title'),
                                                  tester=config.get_value('report', 'tester'),)
        runner.run(suite)


