#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/7/25 20:50
from scripts.payment import Payment
import unittest
from unittest import mock


class TestPayment(unittest.TestCase):
    def setUp(self):
        self.payment = Payment()

    def test_success(self):
        self.payment.auth = mock.Mock(return_value=200)
        res = self.payment.pay(user_id=1001, card_num=1234, amount=500)
        self.assertEqual('success', res)

    def test_fail(self):
        self.payment.auth = mock.Mock(return_value=500)
        res = self.payment.pay(user_id=1001, card_num=1234, amount=500)
        self.assertEqual('fail', res)

    def test_retry_success(self):
        self.payment.auth = mock.Mock(side_effect=[TimeoutError, 200])
        res = self.payment.pay(user_id=1001, card_num=1234, amount=500)
        self.assertEqual('success', res)

    def test_retry_fail(self):
        self.payment.auth = mock.Mock(side_effect=[TimeoutError, 500])
        res = self.payment.pay(user_id=1001, card_num=1234, amount=500)
        self.assertEqual('fail', res)


if __name__ == '__main__':
    unittest.main()



