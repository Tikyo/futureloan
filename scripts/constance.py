#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : Tikyo
# @Datetime : 2019-07-14 09:52
import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONF_PATH = os.path.join(BASE_DIR, 'configs')
SETTING_CONF_PATH = os.path.join(CONF_PATH, 'setting.conf')
NEW_CONF_PATH = os.path.join(CONF_PATH, 'new_setting.conf')
DATA_PATH = os.path.join(BASE_DIR, 'datas')
CASES_PATH = os.path.join(DATA_PATH, 'cases.xlsx')
REPORT_PATH = os.path.join(BASE_DIR, 'reports')
LOG_PATH = os.path.join(BASE_DIR, 'logs')
CASE_PATH = os.path.join(BASE_DIR, 'cases')

if __name__ == '__main__':
    print(BASE_DIR)
    print(CONF_PATH)
    print(SETTING_CONF_PATH)
    print(REPORT_PATH)
