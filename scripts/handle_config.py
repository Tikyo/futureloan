#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/27 21:22
from configparser import ConfigParser
from scripts.constance import NEW_CONF_PATH
from scripts.constance import SETTING_CONF_PATH


class HandleConfig:
    def __init__(self, conf_file_name):
        self.config = ConfigParser()
        self.file_name = conf_file_name
        self.config.read(self.file_name, encoding='utf-8')

    def get_value(self, session, key):
        value = self.config.get(session, key)
        return value

    def get_int(self, session, key):
        int_value = self.config.getint(session, key)
        return int_value

    def get_boolean(self, session, key):
        boolean_value = self.config.getboolean(session, key)
        return boolean_value

    def write_to_config(self, session, key, value):
        if self.config.has_section(session):
            pass
        else:
            self.config.add_section(session)
        self.config.set(session, key, value)
        with open(self.file_name, 'w') as f:
            self.config.write(f)


config = HandleConfig(SETTING_CONF_PATH)
new_config = HandleConfig(NEW_CONF_PATH)
if __name__ == '__main__':
    newconfig = HandleConfig(NEW_CONF_PATH)
    newconfig.write_to_config('test', 'test', '123')
