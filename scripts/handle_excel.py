#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/26 19:11
from openpyxl import load_workbook
from scripts.handle_config import config


class HandleExcel:
    def __init__(self, file_name, sheet_name=None):
        self.file_name, self.sheet_name = file_name, sheet_name
        self.sheet_head = None

    def get_cases(self):
        wb = load_workbook(self.file_name)
        if self.sheet_name is None:
            ws = wb.active
        else:
            ws = wb[self.sheet_name]

        sheet_head_tuple = tuple(ws.iter_rows(min_row=1, max_row=1, values_only=True))
        self.sheet_head = sheet_head_tuple[0]

        sheet_content = tuple(ws.iter_rows(min_row=2, max_row=ws.max_row, values_only=True))
        case_list = []
        for data in sheet_content:
            dict1 = dict(zip(self.sheet_head, data))
            case_list.append(dict1)
        return case_list

    def write_result(self, row, actual, result):
        # 可以跟get_cases共用wb吗？--不可，前面写入的数据可能没有保存
        my_wb = load_workbook(self.file_name)
        if self.sheet_name is None:
            my_ws = my_wb.active
        else:
            my_ws = my_wb[self.sheet_name]
        if isinstance(row, int) and (2 <= row <= my_ws.max_row):
            my_ws.cell(row=row, column=config.get_int('excel', 'actual_col'), value=actual)
            my_ws.cell(row=row, column=config.get_int('excel', 'result_col'), value=result)
            my_wb.save(self.file_name)
            my_wb.close()
        else:
            print('传入行号有误')


if __name__ == '__main__':
    HandleExcel('cases.xlsx').get_cases()
