import re
from scripts.handle_mysql import HandleMysql
from scripts.handle_config import HandleConfig
from scripts.constance import NEW_CONF_PATH


class Context:
    not_existed_phone_pattern = r'\$\{not_existed_tel\}'
    existed_phone_pattern = r'\$\{existed_tel\}'
    investor_phone_pattern = r'\$\{investor_tel\}'
    existed_member_id_pattern = r'\$\{member_id\}'
    not_existed_member_id_pattern = r'\$\{not_existed_member_id\}'
    admin_user_tel_pattern = r'\$\{admin_user_tel\}'
    lender_tel_pattern = r'${lender_tel}'
    lender_id_pattern = r'\$\{lender_id\}'
    loan_id_pattern = r'\$\{loan_id\}'
    investor_id_pattern = r'\$\{investor_id\}'

    def __init__(self):
        pass

    @classmethod
    def not_existed_phone_replace(cls, data):
        """
        替换未注册的手机号
        :return:
        """
        mysql = HandleMysql()
        if re.search(cls.not_existed_phone_pattern, data):
            new_phone = mysql.get_phone_unregister()
            data = re.sub(cls.not_existed_phone_pattern, new_phone, data)
        mysql.close()
        return data

    @classmethod
    def existed_phone_replace(cls, data):
        mysql = HandleMysql()
        if re.search(cls.existed_phone_pattern, data):
            existed_phone = mysql.get_phone_registered()
            data = re.sub(cls.existed_phone_pattern, existed_phone, data)
        mysql.close()
        return data

    @classmethod
    def investor_tel_replace(cls, data):
        my_config = HandleConfig(NEW_CONF_PATH)
        if re.search(cls.investor_phone_pattern, data):
            investor_tel = my_config.get_value('investor_user', 'mobilephone')
            data = re.sub(cls.investor_phone_pattern, investor_tel, data)
        return data

    @classmethod
    def admin_tel_replace(cls, data):
        my_config = HandleConfig(NEW_CONF_PATH)
        if re.search(cls.admin_user_tel_pattern, data):
            admin_user_tel = my_config.get_value('admin_user', 'mobilephone')
            data = re.sub(cls.admin_user_tel_pattern, admin_user_tel, data)
        return data

    @classmethod
    def lender_tel_replace(cls, data):
        my_config = HandleConfig(NEW_CONF_PATH)
        if re.search(cls.lender_tel_pattern, data):
            lender_tel = my_config.get_value('lender', 'mobilephone')
            data = re.sub(cls.lender_tel_pattern, lender_tel, data)
        return data

    @classmethod
    def existed_member_id_replace(cls, data):
        mysql = HandleMysql()
        if re.search(cls.existed_member_id_pattern, data):
            existed_member_id = mysql.get_existed_member_id()
            data = re.sub(cls.existed_member_id_pattern, str(existed_member_id), data)
        mysql.close()
        return data

    @classmethod
    def not_existed_member_id_replace(cls, data):
        mysql = HandleMysql()
        if re.search(cls.not_existed_member_id_pattern, data):
            not_existed_member_id = mysql.get_not_existed_member_id()
            data = re.sub(cls.not_existed_member_id_pattern, str(not_existed_member_id), data)
        mysql.close()
        return data

    @classmethod
    def lender_id_replace(cls, data):
        my_config = HandleConfig(NEW_CONF_PATH)
        if re.search(cls.lender_id_pattern, data):
            lender_id = my_config.get_value('lender', 'id')
            data = re.sub(cls.lender_id_pattern, lender_id, data)
        return data

    @classmethod
    def loan_id_replace(cls, data):
        if re.search(cls.loan_id_pattern, data):
            loan_id = getattr(cls, 'loan_id')
            data = re.sub(cls.loan_id_pattern, str(loan_id), data)
        return data

    @classmethod
    def investor_id_replace(cls, data):
        my_config = HandleConfig(NEW_CONF_PATH)
        if re.search(cls.investor_id_pattern, data):
            investor_id = my_config.get_value('investor_user', 'id')
            data = re.sub(cls.investor_id_pattern, investor_id, data)
        return data

    @classmethod
    def register_data_replace(cls, data):
        data = cls.not_existed_phone_replace(data)
        data = cls.existed_phone_replace(data)
        return data

    @classmethod
    def login_data_replace(cls, data):
        data = cls.not_existed_phone_replace(data)
        data = cls.existed_phone_replace(data)
        return data

    @classmethod
    def recharge_data_replace(cls, data):
        data = cls.investor_tel_replace(data)
        data = cls.not_existed_phone_replace(data)
        return data

    @classmethod
    def add_data_replace(cls, data):
        data = cls.admin_tel_replace(data)
        data = cls.lender_id_replace(data)
        data = cls.not_existed_member_id_replace(data)
        return data

    @classmethod
    def invest_data_replace(cls, data):
        data = cls.admin_tel_replace(data)
        data = cls.lender_id_replace(data)
        data = cls.loan_id_replace(data)
        data = cls.investor_tel_replace(data)
        data = cls.investor_id_replace(data)
        return data


if __name__ == '__main__':
    # data1 = "{mobilephone': ‘${existed_tel}’, 'pwd': '123456', 'regname': 'lemon'}"
    # data1 = Context.existed_phone_replace(data1)
    # print(data1)

    data2 = "{mobilephone': ‘${admin_user_tel}’, 'pwd': '123456', 'regname': 'lemon'}"
    data2 = Context.invest_data_replace(data2)
    print(data2)
