#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/7/25 20:34
import requests


class Payment:
    def __init__(self):
        self.res = None

    def auth(self, card_num, amount):
        """
        请求第三方支付接口，返回响应状态码
        :param card_num: 卡号
        :param amount: 金额
        :return: 200 支付成功 500支付异常
        """
        url = 'http://第三方支付接口'
        data = {"card_num": card_num, "amount": amount}
        self.res = requests.post(url, data)
        return self.res.status_code

    def pay(self, user_id, card_num, amount):
        try:
            status_code = self.auth(card_num, amount)
        except TimeoutError:
            print('超时')
            status_code = self.auth(card_num, amount)
        if status_code == 200:
            print('[{}]支付[{}]成功。进行扣款并登记支付记录。'.format(user_id, amount))
            return 'success'
        elif status_code == 500:
            print('[{}]支付[{}]失败。'.format(user_id, amount))
            return 'fail'


if __name__ == '__main__':
    payment = Payment()
    payment.pay(user_id=1001, card_num=1234, amount=500)
