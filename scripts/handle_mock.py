#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/7/25 20:24
import requests
from unittest import mock


def request_lemfix():
    return requests.get('http://lemfix.com/').text


def print_content():
    print(request_lemfix())


if __name__ == '__main__':
    request_lemfix = mock.Mock(return_value='haha')
    print_content()
