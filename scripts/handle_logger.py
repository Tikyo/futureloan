#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author : Tikyo
# @Time   : 2019/6/30 15:08
import logging
from scripts.handle_config import config
from scripts.constance import LOG_PATH
import time


def handle_logger():
    log = logging.getLogger(config.get_value('log', 'logger_name'))
    log.setLevel(config.get_value('log', 'logger_level'))

    console_handle = logging.StreamHandler()
    path = LOG_PATH + '/' + config.get_value('file path', 'log_path') + '.' + time.strftime('%Y%m%d') + '.log'
    file_handle = logging.FileHandler(path, encoding='utf-8')
    simple_format = logging.Formatter(config.get_value('log', 'simple_format'))
    verbose_format = logging.Formatter(config.get_value('log', 'verbose_format'))

    console_handle.setFormatter(simple_format)
    file_handle.setFormatter(verbose_format)

    console_handle.setLevel(config.get_value('log', 'console_level'))
    file_handle.setLevel(config.get_value('log', 'log_level'))

    log.addHandler(console_handle)
    log.addHandler(file_handle)
    return log


logger = handle_logger()

if __name__ == '__main__':
    logger = handle_logger()
    logger.info('jajj')


