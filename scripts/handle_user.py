#!/user/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : Tikyo
# @Datetime : 2019-07-16 20:34
from scripts.handle_requests import HttpRequests
from scripts.handle_mysql import HandleMysql
from scripts.handle_context import Context
from scripts.handle_logger import logger
from scripts.handle_config import new_config


class HandleUser:
    def __init__(self):
        self.my_request = HttpRequests()
        self.mysql = HandleMysql()

    def register_user(self, user_name):
        url = 'http://tj.lemonban.com/futureloan/mvc/api/member/register'
        data = '{"mobilephone": "${not_existed_tel}", "pwd": "123456", "regname": "%s"}' % user_name
        # 参数替换
        new_data = Context.not_existed_phone_replace(data)
        resp = self.my_request.to_request('POST', url, new_data)
        return resp.text

    def register_3_users_and_save(self):
        self.register_user('investor_user')
        self.register_user('admin_user')
        self.register_user('lender')
        self.save_member_to_conf('investor_user')
        self.save_member_to_conf('admin_user')
        self.save_member_to_conf('lender')

    def save_member_to_conf(self, name):
        """
        获取member信息，保存到配置文件
        :param name:
        :return:
        """
        member_info = self.mysql.get_member_info(name)
        if not member_info:
            logger.error('用户名称不存在')
        else:
            for key in member_info.keys():
                conf_key = key
                conf_value = str(member_info[key])
                new_config.write_to_config(name, conf_key, conf_value)

    def close(self):
        self.my_request.close()
        self.mysql.close()


user = HandleUser()
if __name__ == '__main__':
    # print(user.register_user('test_user'))
    user.register_3_users_and_save()
